
const myApiKey = `a4a74a4e6e38b5d0ce383caae74fdd59`;
var myHeaders = new Headers();
myHeaders.append("x-rapidapi-key", myApiKey);
myHeaders.append("x-rapidapi-host", "v3.football.api-sports.io");

var requestOptions = {
  method: 'GET',
  headers: myHeaders,
  redirect: 'follow'
};

fetch("https://v3.football.api-sports.io/teams/statistics?season=2019&team=33&league=39", requestOptions)
  .then(response => response.json())
  .then(result => {

    //Show name of Team Club Data
    const name = document.querySelector('.team-name');
    name.innerHTML = `
        <h3>${result.response.team.name}</h3>
    `;

    //Show iamge of team Club Data
    const image = document.querySelector('.team-images');
    
    image.innerHTML= `
        <img src=${result.response.team.logo} alt="logo" width="60" height="60">
    `;

    //Show table Static of Team Club Data
    const teamTable = document.querySelector('.team-static-table');
    teamTable.innerHTML = 
    `
    <thead>
    <tr>
      <th scope="col"></th>
      <th scope="col">Home</th>
      <th scope="col">Away</th>
      <th scope="col">All</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">Game Played</th>
      <td>${result.response.fixtures.played.home}</td>
      <td>${result.response.fixtures.played.away}</td>
      <td>${result.response.fixtures.played.total}</td>
    </tr>
    <tr>
      <th scope="row">Win</th>
      <td>${result.response.fixtures.wins.home}</td>
      <td>${result.response.fixtures.wins.away}</td>
      <td>${result.response.fixtures.wins.total}</td>
    </tr>
    <tr>
      <th scope="row">Draw</th>
      <td>${result.response.fixtures.draws.home}</td>
      <td>${result.response.fixtures.draws.away}</td>
      <td>${result.response.fixtures.draws.total}</td>
    </tr>
    <tr>
      <th scope="row">Loses</th>
      <td>${result.response.fixtures.loses.home}</td>
      <td>${result.response.fixtures.loses.away}</td>
      <td>${result.response.fixtures.loses.total}</td>
    </tr>
    <tr>
      <th scope="row">Goals</th>  
      <td colspan="3"></td>
    </tr>
    <tr>
      <th scope="row">Goals For</th>  
      <td>${result.response.goals.for.total.home}</td>
      <td>${result.response.goals.for.total.away}</td>
      <td>${result.response.goals.for.total.total}</td>
    </tr>
    <tr>
      <th scope="row">Goals Against</th>  
      <td>${result.response.goals.against.total.home}</td>
      <td>${result.response.goals.against.total.away}</td>
      <td>${result.response.goals.against.total.total}</td>
    </tr>
  </tbody>
    `;
  })
  .catch(error => console.log('error', error));